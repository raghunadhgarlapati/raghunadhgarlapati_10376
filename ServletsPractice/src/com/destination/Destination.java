package com.destination;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

/**
 * Servlet implementation class Destination
 */
@WebServlet("/Destination")
public class Destination extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public Destination() {
        super();
        // TODO Auto-generated constructor stub
    }

	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		//create printwriter		
		PrintWriter out = response.getWriter();
		//creating a session
		HttpSession httpSession=request.getSession();
		//display the details
		String name=(String) request.getParameter("uname");
		int id=Integer.parseInt(request.getParameter("uid"));
		int age=Integer.parseInt(request.getParameter("age"));
		out.println("name: "+name+"Id: "+id+"age "+age);
		
		 
	}

}
