package com.source;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Source
 */
@WebServlet("/Source")
public class Source extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
	        //printswriter object
		PrintWriter printWriter=response.getWriter();
	  printWriter.println("<html><body>");
	  printWriter.println("<a href='Destination'>see the details</a>");
	  printWriter.println("</body></html>");
		//creating a session
	  HttpSession httpSession=request.getSession();
			//request forwarding to another servlet
		RequestDispatcher rd=request.getRequestDispatcher("Destination");
		rd.forward(request, response);
		
		
		
	}

}
