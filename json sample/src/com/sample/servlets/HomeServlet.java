package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * this servlet method is used to get the request from the html
	 * page and sends back the response to the user in the form of html
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String name = request.getParameter("user");
		String pwd = request.getParameter("pwd");
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html><body bgcolor='cyan'>");
		if (pwd.equals("admin123")) {
			pw.println("<h2> Hello :" + name + "</h2>");
		} else {
			pw.println("<h2> Invalid User" + "</h2>");
		}

		pw.println("</body></html>");

		pw.close();

	}

}
