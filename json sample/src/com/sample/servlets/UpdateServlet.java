package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    
    /**
	 * this servlet method is used for updating and to get the request from the
	 * html page and sends back the response to the user in the form of html
	 */
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
			
		Student student = new Student();
		student.setId(studentId);
		student.setName(studentName);
        student.setAge(studentAge);
		
		StudentService service = new StudentServiceImpl();
		
		boolean result = service.updateStudent(student);
				
		PrintWriter out = response.getWriter();
		
		out.print("<html><body>");
		
		if(result) {
			out.print("<h2>Student updated</h2>");
		}
		else {
			out.print("<h2>Something wrong</h2>");
		}
		
		out.print("</body></html>");
		out.close();
	}

}
