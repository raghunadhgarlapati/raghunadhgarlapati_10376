package com.abc.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.abc.entities.Account;
import com.abc.util.HibernateUtil;


public class AccountMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Account account = new Account();
		account.setAccno(1);
		account.setName("FristAccount");
		account.setBalance(50000);
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.save(account);
		System.out.println("Acount saved");
		txn.commit();
		session.close();
		HibernateUtil.shutdown();
		
//		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
//		
//		Session session = sessionFactory.openSession();
//		
//		Account account = session.get(Account.class, 1);
//		session.close();
//		
//		Session session1 = sessionFactory.openSession();
//		Account account1 = session1.get(Account.class, 1);
//		session1.close();
//
//		
//		if(account==account1) {
//			System.out.println("same object");
//		}
//		else {
//			System.out.println("different object");	
//		}
//		
			
		HibernateUtil.shutdown();
		

	}

}
