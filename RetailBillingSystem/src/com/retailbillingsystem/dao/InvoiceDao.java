package com.retailbillingsystem.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.retailbillingsystem.bean.Invoice_details;
import com.retailbillingsystem.util.DBUtil;

/**
 * This class is used to perform invoice DB operations
 * @author Batch-A
 *
 */
public class InvoiceDao {

	/**
	 * This method is used to fetch invoices
	 * @return list of invoices
	 */
	public static List<Invoice_details> readFromTable_invoice()
	{
		Connection connection=DBUtil.getconn();
		ResultSet resultSet=null;
		List<Invoice_details> invoiceDetails = new ArrayList<Invoice_details>();
		try {
			//initializing statement object
			Statement statement=connection.createStatement();
			//calling method to execute query
			resultSet=statement.executeQuery("select * from invoice_tab");
			//adding details fetched from database to the list
			while(resultSet.next())
			{
				Invoice_details  billing_details = new Invoice_details();
				billing_details.setInvoiceno(resultSet.getInt(1));
				billing_details.setProduct_name(resultSet.getString(4));
				billing_details.setQuantity(resultSet.getDouble(2));
				billing_details.setPrice(resultSet.getDouble(3));
				invoiceDetails.add(billing_details);
			}
		} catch (SQLException e) {
			
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//returning list
		return invoiceDetails;
	}
	
	/**
	 * This method is used to insert previous bill details into database. This is used whenever the bill is generated.
	 * @param invoice_num
	 * @param total
	 * @param discount
	 * @param tax
	 */
	public void invoiceDetails(int invoice_num,double total,double discount,double tax) {
		
		Connection con = null;
		try {
			//calling the method to establish connection
			con = DBUtil.getconn();
			
			//executing insert query by using prepared statement 
			String sql="insert into biller_tab values(?,?,?,?)";
			PreparedStatement pst= con.prepareStatement(sql);
			
			//setting the values into table
			pst.setInt(1, invoice_num);
			pst.setDouble(2, discount);
			pst.setDouble(3, tax);
			pst.setDouble(4, total);
			pst.executeUpdate(); 
						
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * This method is used to fetch invoice numbers
	 * @return list of invoice numbers
	 */
	public  static List<Integer> readFromTable_InvoicenNumbers() 
	{
		Connection connection=DBUtil.getconn();
		ResultSet resultSet=null;
		List<Integer> invoiceNumbers = new ArrayList<Integer>();
		try {
			String sql="select  distinct invoicenumber from invoice_tab";
			//initializing statement object
			Statement statement=connection.createStatement();
			//calling method to execute query
			resultSet=statement.executeQuery(sql);
			//adding details fetched from database to the list
			while(resultSet.next())
			{
				invoiceNumbers.add(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//returning list
		return invoiceNumbers;
		
	}
}
