package com.retailbillingsystem.services;

import java.util.List;

import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.dao.ProductsDao;

/**
 * This class allows stocker to perform his operations
 * @author Batch-A
 *
 */
public class StockerServices {
	/**
	 * This method is used to add products into database
	 * @param productname
	 * @param quantity
	 * @param price
	 * @param productType
	 */
	public boolean addProduct(String productname,double quantity,double price,String productType)
	{
		 boolean flag=false;
		 ProductsDao writingData = new ProductsDao();
		
		 
		 flag=writingData.productsInsertion(productname, quantity, price, productType);
		 return flag;
	}
	/**
	 * This method is used to display products 
	 * @return  productlist
	 */
	public List<Products> displayProducts()
	{
		return ProductsDao.readFromTable_products();
	}
	/**
	 * This method is used for deleting particular data in database 
	 * @param productName
	 * @return boolean value
	 */
	public boolean deleteProduct(String productName)
	{
		ProductsDao delete=new ProductsDao();
		return delete.deleteFromTable_products(productName); 
    }
	
	/**
	 * This method is used for updating products in database table 
	 * @param productName
	 * @param quantity
	 * @param operation
	 * @return message
	 */
	public String updateProductQuantity(String productName,double quantity,String operation)
	{
		List<Products> products = displayProducts();
		boolean flag = false;
		//checking product quantity and updating product quantity
		for(Products product : products)
		{
			if(product.getProductname().equalsIgnoreCase(productName)) {
				ProductsDao manipulatingData = new ProductsDao();
				if(quantity > product.getQuantity() && operation.equals("2") ) {
					return "Quantity should be less than stock to decrease" ;
				}
				if(operation.equals("2"))
				{
					quantity=(-1)*quantity;
				}
				
			
				quantity = quantity + product.getQuantity();
				
				flag = manipulatingData.updateTable_products_quantity(productName, quantity);
			}
		}
		if(flag) {
			
		return "Product Updated Successfully......";
    }
		return "Product updation failed........";
	}
	/**
	 * This method is used for updating product price in database table
	 * @param productName
	 * @param price
	 * @param operation
	 * @return
	 */
	public String updateProductPrice(String productName,double price,String operation)
	{
		List<Products> products = displayProducts();
		boolean flag = false;
		//checking product price and updating product price
		for(Products product : products)
		{
			if(product.getProductname().equalsIgnoreCase(productName)) {
				ProductsDao manipulatingData = new ProductsDao();
				if(price > product.getQuantity() && operation.equals("2") ) {
					return "Price should be less than stock to decrease" ;
				}
				if(operation.equals("2"))
				{
					price=(-1)*price;
				}
				
			
				price = price + product.getQuantity();
				
				flag = manipulatingData.updateTable_products_price(productName, price);
			}
		}
		if(flag) {
			
		return "Product Updated Successfully......";
    }
		return "Product updation failed........";
	}

}
