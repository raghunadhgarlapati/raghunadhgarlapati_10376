package com.retailbillingsystem.controller;

import java.util.Scanner;

/***
 * This class is used as a main method for controlling whole project
 * @author BATCH A
 *
 */
public class BillingMain {

	
	public static void main(String[] args) {
		// First Window
				boolean choice = true;
				// main loop

				@SuppressWarnings("resource")
				Scanner sc1 = new Scanner(System.in);// creating scanner object
				mainLoop: while (choice) {

					// department logins
					System.out.println(
							"*****************************************************************************\n\t\t\tINNOMINDS QUALITY STORE\n*****************************************************************************\n1.Manager department Login \n2.Billing Department Login \n3.Stock department Login \n4.Exit\nWhich department do you want to login?");

					String operation = "";
					try {
						operation = sc1.nextLine();
						System.out.println("==============================================================================");
					} catch (Exception e) {
						System.err.println("please enter only integer choices\n");
						return;
					}
					LoginsController controller = new LoginsController();
					switch (operation)// performing user' required operations
					{
					case "1":// Manager's Login with credentials
						
						controller.managerLogin();

						break;

					case "2":// Biller's Login with credentials
						controller.billerLogin();// calling login method for biller
						break;

					case "3":// Stocker's Login with credentials// calling login method for biller
						controller.stockerLogin();
						break;

					case "4":// exit from menu
						System.out.println(
								"\t\t\tThank You Visit Again!!!!\n==============================================================================");
						break mainLoop;
					default:
						System.out.println("Enter a valid input");

					}
				}
	}

}