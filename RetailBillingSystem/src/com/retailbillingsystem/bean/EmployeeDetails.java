package com.retailbillingsystem.bean;


/***
 * This class is an object to store employee details
 * @author BATCH A
 *
 */
public class EmployeeDetails  {
	
	//initializing the variables
	private int employeeId;
	private String employeeName;
	private String employeePassword;
	private String role;
	private String securityQuestion;
	
	/***
	* This method is for getting the employee id of the current employee
	* @return employeeId
	*/
	public int getEmployeeId() {
		return employeeId;
	}
	
	/***
	* This method is for setting the employee id of the current employee
	* @param employeeId
	*/
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	/***
	* This method is for getting the employeeName of the current employee
	* @return employeeName
	*/
	public String getEmployeeName() {
		return employeeName;
	}
	
	/***
	* This method is for setting the employeeName of the current employee
	* @param employeeName
	*/
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	
	/***
	* This method is for getting the employeePassword of the current employee
	* @return employeePassword
	*/
	public String getEmployeePassword() {
		return employeePassword;
	}
	
	/***
	* This method is for setting the employeePassword of the current employee
	* @param employeePassword
	*/
	public void setEmployeePassword(String employeePassword) {
		this.employeePassword = employeePassword;
	}
	
	/***
	* This method is for getting the employee role of the current employee
	* @return role
	*/
	public String getRole() {
		return role;
	}
	
	/***
	* This method is for setting the employee role of the current employee
	* @param role
	*/
	public void setRole(String role) {
		this.role = role;
	}
	
	/***
	* This method is for getting the securityQuestion of the current employee
	* @return securityQuestion
	*/
	public String getSecurityQuestion() {
		return securityQuestion;
	}
	
	/***
	* This method is for setting the securityQuestion of the current employee
	* @param securityQuestion
	*/
	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}
	@Override
	public String toString() {
		return "EmployeeDetails [employeeId=" + employeeId + ", employeeName=" + employeeName + ", employeePassword="
				+ employeePassword + ", role=" + role + ", securityQuestion=" + securityQuestion + "]";
	}
	
}

