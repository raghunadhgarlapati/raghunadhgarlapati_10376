package com.bean;

public class StudentDetails {
	//variables declaring as private
	private int studentId;
	private String studentName;
	private int studentAge;
	
	//setters and getters
	public int getStudentid() {
		return studentId;
	}
	public void setStudentid(int studentid) {
		this.studentId = studentid;
	}
	public String getStudentname() {
		return studentName;
	}
	public void setStudentname(String studentname) {
		this.studentName = studentname;
	}
	public int getAge() {
		return studentAge;
	}
	public void setAge(int age) {
		this.studentAge = age;
	}
	

}
