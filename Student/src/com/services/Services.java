package com.services;

import java.util.ArrayList;
import java.util.List;

import com.bean.StudentDetails;
import com.dao.StudentsDao;



public class Services {
	/**
	 * method is used to add the student into the list
	 * @param id
	 * @param name
	 * @param age
	 * @return
	 */
	public static boolean addStudent(int id,String name,int age)
	{
		boolean flag=false;
		StudentsDao  dao=new StudentsDao();
		flag=dao.addStudent(id,name,age);
		return flag;
	}
     
	/**
	 * method is used to display the studentdetails  from the database table
	 * return details
	 */
	public static List<StudentDetails> display()
	{
		List<StudentDetails>details=new ArrayList<StudentDetails>();
		StudentsDao studentsDao=new StudentsDao();
		details= studentsDao.display();
		return details;
		
	}
	
	public static boolean deleteById(int studentId) {
		StudentsDao studentsDao=new  StudentsDao();
		boolean result = studentsDao.deleteById(studentId);
		
		return result;
	}

	/**
	 * this method is used to update the student details
	 * @param student
	 * @return
	 */

	public static boolean updateStudent(StudentDetails student) {
		StudentsDao studentDAO = new StudentsDao();
		boolean result = studentDAO.updateStudent(student);
		
		return result;
	}

	/**
	 * used to get the records from the table of the entered database
	 * @param studentId
	 * @return
	 */
	public static List<StudentDetails> serachById(int studentId) {
		List<StudentDetails> studentDetails=new ArrayList<StudentDetails>();
		StudentsDao dao=new StudentsDao();
		studentDetails=dao.serachById(studentId);
		return studentDetails;
		
		
	}
}
