package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.StudentDetails;

import com.services.Services;

/**
 * Servlet implementation class SearchById
 */
@WebServlet("/SearchById")
public class SearchById extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchById() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * used to get the request from html page for display the records of entered id from student table
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int studentId = Integer.parseInt(request.getParameter("s_id"));
		List<StudentDetails> studentDetails=new ArrayList<StudentDetails>();
		
		studentDetails= Services.serachById(studentId);
		PrintWriter printWriter=response.getWriter();
		printWriter.println("<html><body>");
		if(studentDetails.isEmpty() )
		{
			printWriter.println("no such id details found");
		}
		else
		{
			printWriter.println("<table border='1'>");
			printWriter.println("<tr><th>id</th><th>name</th><th>age</th></tr>");
			for(StudentDetails e:studentDetails)
			{
				printWriter.println("<tr>");
				printWriter.println("<td>"+e.getStudentid()+"</td>"+"<td>"+e.getStudentname()+"</td>"+"<td>"+e.getAge()+"</td>");
				printWriter.println("</tr>");
			}
			printWriter.println("</table>");
			printWriter.println("</body></html>");
			printWriter.close();
			
		}
		
	}

	

}
