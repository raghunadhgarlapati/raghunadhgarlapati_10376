package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.services.Services;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class AddEmployee extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddEmployee() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * get the request from html to add the record to the student details in the student table
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean flag=false;
	  int 	studentId = Integer.parseInt(request.getParameter("StudentId"));
	 String	studentName = request.getParameter("StudentName");
	 int 	studentAge = Integer.parseInt(request.getParameter("StudentAge"));
	 flag=Services.addStudent(studentId,studentName,studentAge);
	  PrintWriter printWriter= response.getWriter();
	  printWriter.println("<html><body>");
	  if(flag)
	  {
		  printWriter.print("<h2>employee added Sucessdully<h2>");
	  }
	  else
	  {
		  printWriter.println("<h2>employee added failed!!!!</h2>");
	  }
	  printWriter.println("<body><html>");
	  
	  
	 
	}

	

}
