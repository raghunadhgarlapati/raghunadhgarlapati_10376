package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.StudentDetails;
import com.services.Services;

/**
 * Servlet implementation class Delete
 */
@WebServlet("/Delete")
public class DeleteStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * used to get the request from html page for deleteing the record in student table
	 */
    //to get the data from html
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		int studentId = Integer.parseInt(request.getParameter("s_id"));
	
		StudentDetails studentDetails=new StudentDetails();//object creation
		studentDetails.setStudentid(studentId);
		
		boolean result = Services.deleteById(studentId);
		
		PrintWriter out = response.getWriter();
		
		out.print("<html><body>");
		
		if(result) {
		
			out.print("<h2>Student deleted</h2>");
		}
		else {
			//used to move to student delete page
			RequestDispatcher dispatcher=request.getRequestDispatcher("DeleteStudent.html");
			dispatcher.include(request, response);
			out.print("<h2>Id not found</h2>");
		}
		
		out.print("</body></html>");
		out.close();
	}


}
