package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.List;

import com.bean.StudentDetails;
import com.util.DBConnection;

public class StudentsDao {
	/**
	 * this method is used add the student details in the database
	 * @param id
	 * @param name
	 * @param age
	 * @return
	 */


	public boolean addStudent(int id, String name, int age) {
		boolean flag=false;
		Connection connection=DBConnection.getConnection();
		String sql="insert into student values(?,?,?)";
		try {
			//executing the query with the prepared statement 
			PreparedStatement preparedStatement=connection.prepareStatement(sql);
			preparedStatement.setInt(1, id);
			preparedStatement.setString(2, name);
			preparedStatement.setInt(3, age);
			int n=preparedStatement.executeUpdate();
			if(n>0)
			{
			flag=true;
			}
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return flag;
		
	}
	/**
	 * this method is used to display the student details
	 * @return list 
	 */
	public List<StudentDetails> display()
	{
		 List<StudentDetails> daos= new ArrayList<StudentDetails>();
		//executing the query with the prepared statement 
		Connection connection=DBConnection.getConnection();
		String sql="Select *from student";
		Statement statement;
		try {
			statement = connection.createStatement();
			ResultSet resultSet=statement.executeQuery(sql);
	
			while(resultSet.next())
			{
				StudentDetails studentDetails=new StudentDetails();
				studentDetails.setStudentid(resultSet.getInt(1));
				studentDetails.setStudentname(resultSet.getString(2));
				studentDetails.setAge(resultSet.getInt(3));
				daos.add(studentDetails);
			}
			connection.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return daos;
	}
	/**
	 * this method is used to delete the record from the database of the entered id
	 * @param id
	 * @return results
	 */
	public boolean deleteById(int id) {
		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "DELETE FROM student WHERE StudentId = ?";
		try {
			//get connection
			con = DBConnection.getConnection();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
			con.close();
		}
		catch (SQLException e){
			
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
		
		
	}
	/**
	 *  this method is used to  update the student details
	 * @param student
	 * @return
	 */
	public boolean updateStudent(StudentDetails student) {
		Connection con = null;
		PreparedStatement ps = null;
		
		int id = student.getStudentid();
		String name =student.getStudentname();
		int age = student.getAge();
		boolean result = false;
		//update query
		String sql = "update student SET name = ?, studentage = ? where studentid = ?";
		try {
			con = DBConnection.getConnection();
			System.out.println("");
			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, age);
			ps.setInt(3, id);
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
			con.close();
			
			
		}
		catch (SQLException e){
			System.out.println(e);
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	/**
	 * this method is used to display the entered details
	 * @param studentId
	 * @return
	 */
	public List<StudentDetails> serachById(int studentId) {
		
		List<StudentDetails> daos= new ArrayList<StudentDetails>();
		//executing the query with the prepared statement 
		Connection connection=DBConnection.getConnection();
		String sql="Select *from student where Studentid=?";
		ResultSet resultSet=null;
		PreparedStatement ps = null;
		try {
			ps=connection.prepareStatement(sql);
			
			ps.setInt(1, studentId);
			
			resultSet= ps.executeQuery();
			
	
			while(resultSet.next())
			{
				//creating the object and fetching details
				StudentDetails studentDetails=new StudentDetails();
				studentDetails.setStudentid(resultSet.getInt(1));
				studentDetails.setStudentname(resultSet.getString(2));
				studentDetails.setAge(resultSet.getInt(3));
				daos.add(studentDetails);
			}
			connection.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return daos;
	}

		
		
	

}
