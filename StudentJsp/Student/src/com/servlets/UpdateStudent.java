package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.StudentDetails;
import com.services.Services;



/**
 * Servlet implementation class UpdateStudent
 */
@WebServlet("/UpdateStudent")
public class UpdateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int studentId = Integer.parseInt(request.getParameter("id"));
		String studentName = request.getParameter("name");
		int studentAge = Integer.parseInt(request.getParameter("age"));
			
		StudentDetails student = new StudentDetails();
		student.setStudentid(studentId);
		student.setStudentname(studentName);
        student.setAge(studentAge);
        boolean result = Services.updateStudent(student);
        request.setAttribute("result",result);
		RequestDispatcher dispatcher=request.getRequestDispatcher("UpdateStudent.jsp");
		dispatcher.forward(request, response);
		
		
		
	}
	


}
