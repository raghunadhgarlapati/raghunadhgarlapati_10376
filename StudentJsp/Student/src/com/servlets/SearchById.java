package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.StudentDetails;

import com.services.Services;

/**
 * Servlet implementation class SearchById
 */
@WebServlet("/SearchById")
public class SearchById extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchById() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * used to get the request from html page for display the records of entered id from student table
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int studentId = Integer.parseInt(request.getParameter("s_id"));
		List<StudentDetails> studentDetails=new ArrayList<StudentDetails>();
		
		studentDetails= Services.serachById(studentId);
		RequestDispatcher dispatcher=request.getRequestDispatcher("SerachById.jsp");
		request.setAttribute("studentDetails", studentDetails);
		
		dispatcher.forward(request, response);
		
		
		
		
	}

	

}
