package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.services.Services;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class AddStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * get the request from html to add the record to the student details in the student table
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean flag=false;
	  int 	studentId = Integer.parseInt(request.getParameter("StudentId"));
	 String	studentName = request.getParameter("StudentName");
	 int 	studentAge = Integer.parseInt(request.getParameter("StudentAge"));
	 flag=Services.addStudent(studentId,studentName,studentAge);
	 RequestDispatcher rd=request.getRequestDispatcher("AddStudent.jsp");
	 if(flag)
	 {
		 request.setAttribute("flag", flag);
		 rd.forward(request, response);

	 }
	 else
	 {
	  response.sendRedirect("AddStudent.html");
	 }
	 
	
	  
	  
	 
	}

	

}
