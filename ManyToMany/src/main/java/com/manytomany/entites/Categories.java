package com.manytomany.entites;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class Categories  {

	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private int categoriesId;
	
	@Column(nullable=false)
	private String categoriesName;
	
	
	
	
	 @ManyToMany(fetch = FetchType.LAZY, mappedBy = "categories")
	 private List<Stock>stocks=new ArrayList<Stock>();
 

	public int getCategoriesId() {
		return categoriesId;
	}

	public void setCategoriesId(int categoriesId) {
		this.categoriesId = categoriesId;
	}

	public String getCategoriesName() {
		return categoriesName;
	}

	public void setCategoriesName(String categoriesName) {
		this.categoriesName = categoriesName;
	}
	public List<Stock> getStocks() {
		return stocks;
	}

	public void setStocks(List<Stock> stocks) {
		this.stocks = stocks;
	}
	
	
}
