package com.source;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class Source
 */
@WebServlet("/Source")
public class Source extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//fetching data from html file and storing into varaibles
		String uname=request.getParameter("uname");
		int id=Integer.parseInt(request.getParameter("uid"));
		int age=Integer.parseInt(request.getParameter("age"));
		//creating a session
		HttpSession httpSession=request.getSession();
		//send the request to another session using request dispatcher
		RequestDispatcher rd=request.getRequestDispatcher("Destination.jsp");
		rd.forward(request, response);
		
		
		
	}

}
