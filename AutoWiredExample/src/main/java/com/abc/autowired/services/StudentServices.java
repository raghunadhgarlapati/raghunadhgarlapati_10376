package com.abc.autowired.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.autowired.bean.Student;
import com.abc.autowired.dao.StudentDao;
@Service
public class StudentServices {
	
	
@Autowired
private StudentDao stdDao;

public StudentDao getStdDao() {
	return stdDao;
}

public void setStdDao(StudentDao stdDao) {
	this.stdDao = stdDao;
}

public Student display()
{
	
	
	Student student=stdDao.display();
	return student;
}
public void insert()
{
	stdDao.insert();
}
	
	
}
