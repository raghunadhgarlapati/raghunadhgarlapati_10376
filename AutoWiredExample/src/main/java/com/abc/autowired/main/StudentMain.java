package com.abc.autowired.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.autowired.bean.Student;
import com.abc.autowired.controller.StudentController;



public class StudentMain {


		public static void main(String[] args) {
			
			  ApplicationContext context =  new ClassPathXmlApplicationContext("student.xml");
	 
		         StudentController controller = context.getBean(StudentController.class);
		
				Student  student =controller.display();
				controller.insert();
				
				
			  	System.out.println("StudentDetails");
//			  	System.out.println(student.getName()+" "+student.getAdress().getCity());
			  	
		}

}
