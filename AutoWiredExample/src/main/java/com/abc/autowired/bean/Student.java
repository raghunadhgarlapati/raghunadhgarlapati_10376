package com.abc.autowired.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
@Entity
@Table
@Component
public class Student {
	 
//	 public void setAdress(Adress adress) {
//		this.adress = adress;
//	}
	 @Id
	 @Column
	private int rollno;
	 @Column
	 private String name;
	 @Column
	 private int age;
	 
	// Adress adress;
	public int getRollno() {
		return rollno;
	}
	public void setRollno(int rollno) {
		this.rollno = rollno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
//	public Adress getAdress() {
//		return adress;
//	}
//	 

}
