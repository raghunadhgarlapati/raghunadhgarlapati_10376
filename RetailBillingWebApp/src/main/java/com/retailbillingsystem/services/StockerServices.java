package com.retailbillingsystem.services;

import java.util.List;

import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.dao.ProductsDao;

/**
 * This class allows stocker to perform his operations
 * @author Batch-A
 *
 */
public class StockerServices {
	/**
	 * This method is used to add products into database
	 * @param productname
	 * @param quantity
	 * @param price
	 * @param productType
	 */
	public String addProduct(Products product)
	{
		String ret="";
		List<Products> products = ProductsDao.readFromTable_products();
		for(Products productsearch : products)
		{
			if(productsearch.getProductname().equalsIgnoreCase(product.getProductname()))
			{
				ret="Product already exists";
				return ret;
			}
		}
		 
		 ProductsDao writingData = new ProductsDao();
		 boolean flag=writingData.productsInsertion(product);
		 if(flag)
		 {
			 ret="Product added successfully";
		 }
		 else
		 {
			 ret="Product addition failed";
		 }
		 return ret;
	}
	/**
	 * This method is used to display products 
	 * @return  productlist
	 */
	public List<Products> displayProducts()
	{
		return ProductsDao.readFromTable_products();
	}
	/**
	 * This method is used for deleting particular data in database 
	 * @param productName
	 * @return boolean value
	 */
	public boolean deleteProduct(String productName)
	{
		List<Products> deletelist = ProductsDao.readFromTable_products();
		Products deleteProduct=new Products();
		int count=0;
		for(Products product:deletelist)
		{
			if(productName.equalsIgnoreCase(product.getProductname()))
			{
				deleteProduct.setPrice(product.getPrice());
				deleteProduct.setProduct_id(product.getProduct_id());
				deleteProduct.setProductname(product.getProductname());
				deleteProduct.setProductType(product.getProductType());
				deleteProduct.setQuantity(product.getQuantity());
				count++;
			}
		}
		if(count>0)
		{
			ProductsDao delete=new ProductsDao();
			return delete.deleteFromTable_products(deleteProduct); 
		}
		
		else
		{
			return false;
		}
		
    }
	
	/**
	 * This method is used for updating products in database table 
	 * @param productName
	 * @param quantity
	 * @param operation
	 * @return message
	 */
	public String updateProductQuantity(String productName,double quantity,String operation)
	{
		List<Products> products = displayProducts();
		Products productDetails = new Products();
		boolean flag = false;
		int count = 0;
		//checking product quantity and updating product quantity
		for(Products product : products)
		{
			if(product.getProductname().equalsIgnoreCase(productName)) {
				ProductsDao manipulatingData = new ProductsDao();
				if(quantity > product.getQuantity() && operation.equals("2") ) {
					return "Quantity should be less than stock to decrease" ;
				}
				if(operation.equals("2"))
				{
					quantity=(-1)*quantity;
				}
				
			
				quantity = quantity + product.getQuantity();
				
				
				
				productDetails.setProduct_id(product.getProduct_id());
				productDetails.setProductname(productName);
				productDetails.setPrice(product.getPrice());
				productDetails.setQuantity(quantity);
				productDetails.setProductType(product.getProductType());
				flag = manipulatingData.updateTable_products_quantity(productDetails);
				count++;
			}
		}
		if(count==0)
		{
			return "Product Not found";
		}
		if(flag) {
			
		return "Product Updated Successfully......";
    }
		return "Product updation failed........";
	}
	/**
	 * This method is used for updating product price in database table
	 * @param productName
	 * @param price
	 * @param operation
	 * @return
	 */
	public String updateProductPrice(String productName,double price,String operation)
	{
		List<Products> products = displayProducts();
		Products productDetails = new Products();
		boolean flag = false;
		int count = 0;
		//checking product price and updating product price
		for(Products product : products)
		{
			if(product.getProductname().equalsIgnoreCase(productName)) {
				ProductsDao manipulatingData = new ProductsDao();
				if(price > product.getQuantity() && operation.equals("2") ) {
					return "Price should be less than stock to decrease" ;
				}
				if(operation.equals("2"))
				{
					price=(-1)*price;
				}
				
			
				price = price + product.getQuantity();
				
				productDetails.setProduct_id(product.getProduct_id());
				productDetails.setProductname(productName);
				productDetails.setPrice(price);
				productDetails.setQuantity(product.getQuantity());
				productDetails.setProductType(product.getProductType());
				flag = manipulatingData.updateTable_products_price(productDetails);
				count++;
			}
		}
		if(count==0)
		{
			return "Product Not found";
		}
		if(flag) {
			
		return "Product Updated Successfully......";
    }
		return "Product updation failed........";
	}

}
