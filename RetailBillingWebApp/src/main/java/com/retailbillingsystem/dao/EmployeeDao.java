package com.retailbillingsystem.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.retailbillingsystem.bean.EmployeeDetails;

import com.retailbillingsystem.util.HibernateUtil;

/**
 * This class is used to perform employee DB operations
 * @author Batch-A
 *
 */
public class EmployeeDao {
	

	/**
	 * This is used for inserting manager data into table.
	 */
	public void createManager(EmployeeDetails manager)
	{
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		try
		{
			session.save(manager);
			txn.commit();
			session.close();
		} catch (Exception e) {
			
		}

	}

	/**
	 * This is used for inserting employee data into table. This is a manager operation
	 * @param role
	 * @param name
	 * @param password
	 * @param answer
	 * @return
	 */
	public boolean createEmployee(EmployeeDetails employeeDetails) {
		
		boolean flag = false;
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		try
		{
			session.save(employeeDetails);
			txn.commit();
			session.close();
			flag = true;
		} catch (Exception e) {
			
		}
		return flag;
	}
	
	/**
	 * This method is used to fetch employee details
	 * @return list of employee details
	 */
	public static List<EmployeeDetails> fetchEmployeeDetails()
	{
		List<EmployeeDetails> employeeDetails = new ArrayList<EmployeeDetails>();
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Query query = session.createQuery("from EmployeeDetails");
		employeeDetails=query.getResultList();
		return employeeDetails;	
	}
	

	/**
	 * This method is used for deleting employee when required.This is a manager operation.
	 * @param id
	 * @param role
	 * @return true if the employee deleted successfully else false
	 */
	public boolean deleteEmployee(EmployeeDetails employeeDetails) {
		
		boolean flag = false;
		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		try
		{
			session.delete(employeeDetails);
			txn.commit();
			session.close();
			flag = true;
		} catch (Exception e) {
			
		}
		return flag;
		
	}

	
	

}
