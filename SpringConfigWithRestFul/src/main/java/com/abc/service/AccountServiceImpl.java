package com.abc.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.abc.DAO.AccountDAO;
import com.abc.bean.Account;

@Service
public class AccountServiceImpl implements AccountService {
	
	
	@Autowired
	private AccountDAO accountDao;
	
	@Override
	@Transactional
	public Account searchById(int accNo) {
	
		return accountDao.getAccountByAccNo(accNo);
	}

	@Override
	@Transactional
	public List<Account> displayAccountDetails() {
		
		
		return accountDao.displayDetails();
	}

	@Override
	@Transactional
	public boolean addAccount(Account account) {
		
		return  accountDao.createAccount(account);
	}
	@Override
	@Transactional
	public boolean updateAccount(Account account) {
		
		return  accountDao.modifyAccount(account);
	}

	@Override
	@Transactional
	public boolean deleteAccount(Account account) {
		// TODO Auto-generated method stub
		return  accountDao.removeAccount(account);
	}

	
	

}
