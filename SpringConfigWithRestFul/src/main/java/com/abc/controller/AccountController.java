package com.abc.controller;

import java.util.List;

import javax.xml.ws.http.HTTPBinding;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abc.bean.Account;
import com.abc.service.AccountService;


@RestController

public class AccountController {
	
	
	@Autowired
	private AccountService accountService;
	
	@GetMapping("/accountsearch/{id}")
	 public ResponseEntity<Account> get(@PathVariable("id") int id) {
	      Account account = accountService.searchById(id);
	      if (account == null) {
	            
	            return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
	        }
	        return new ResponseEntity<Account>(account, HttpStatus.OK);
	   }
	
	
	
	
	
	@PostMapping("/create")
	public ResponseEntity<?> createNewAccount(@RequestBody Account account) {
		boolean id=accountService.addAccount(account);
		if(id) {
			String body="sucess";
			return new ResponseEntity<>(body,HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<>("fail",HttpStatus.OK);
		}
			
        
			
		}
	
	
	
	
	
	@PostMapping("/accountUpdation")
	public ResponseEntity<?> updateAccount(@RequestBody Account account) {
		
		int accno =	account.getAccno();
		String name=account.getName();
		double balance=account.getBalance();
		
		 account=accountService.searchById(accno);
		
		if(account!=null) {
		
		account.setAccno(accno);
		
		account.setName(name);
	
		account.setBalance(balance);
		String body="sucessfully updated";
		
		return new ResponseEntity<>(body,HttpStatus.OK);
		
		}
		else
		{
			return new ResponseEntity<>("fail to update record",HttpStatus.NOT_FOUND);
		}
		
		
		
		
	}
	
	@DeleteMapping("/accountdelete")
	public ResponseEntity<?>deleteAccount(@RequestBody Account account)
	{
		System.out.println(account.getAccno());
		Account account2=accountService.searchById(account.getAccno());
		if(account2==null)
		{
			return new ResponseEntity<>("id not found to delete",HttpStatus.NOT_FOUND);
		}
		else
		{
			accountService.deleteAccount(account2);
			return new  ResponseEntity<>("Deleted Sucess",HttpStatus.OK);
		}
	}
	
	
	
	
	@GetMapping("/displayAccounts")
	public ResponseEntity<?>displayAccountDetails()
	{
		List<Account> details=accountService.displayAccountDetails();
		if(details.isEmpty())
		{
			return new ResponseEntity<>("No Data Found",HttpStatus.NOT_FOUND);
		}
		else
		{
			return new ResponseEntity<>(details,HttpStatus.OK);
		}
		
	}
}
