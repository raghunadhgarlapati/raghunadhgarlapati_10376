package com.abc.javaconfig;



import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;




	@EnableWebMvc
	@Configuration
	@ComponentScan(basePackages = "com.abc")
	public class SpringConfiguration implements WebMvcConfigurer {

		@Override
		public void addViewControllers(ViewControllerRegistry registry) {
			// TODO Auto-generated method stub
			WebMvcConfigurer.super.addViewControllers(registry);
		}
		   
	}

