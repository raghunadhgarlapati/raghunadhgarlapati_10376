package com.sessionmgmt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class TokenGenrator {
	public boolean createStudent(String token,String username) {		
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		
		//Preparing sql query to insert new student record
		String sql = "insert into token values( ?, ?, ?)";
		try {
			
			//Establishing connection with database
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			
			//setting parameter to sql query
			ps.setInt(1, 1);
			ps.setString(2, token);
			ps.setString(3, username);
			//executing sql query
			int rowsEffected = ps.executeUpdate();
			if(rowsEffected == 1) {
				result = true;
			} 
		}
		catch (SQLException e){
			e.printStackTrace();
		}
		finally {
			try {
				//closing connection with database
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returns true if data inserted successfully otherwise return false
		return result;
	}
	public String displayStudent(String username) {		
		Connection con = null;
		PreparedStatement stmt = null;
		
		String token = null;
		try {
			//Establishing connection with database
			con = DBUtil.getCon();
			String sql = "select * from token where user= ?";
			stmt = con.prepareStatement(sql);
			stmt.setString(1,username);
			ResultSet rs=stmt.executeQuery();
			while(rs.next()) {
				
				token=rs.getString("token");
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	
		finally {
			try {
				//closing connection with database
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returns true if data inserted successfully otherwise return false
		return token;
	}
	public String getToken(String token) {		
		Connection con = null;
		PreparedStatement stmt = null;
		
		String token1 = null;
		try {
			//Establishing connection with database
			con = DBUtil.getCon();
			String sql = "select * from token where token= ?";
			stmt = con.prepareStatement(sql);
			stmt.setString(1,token);
			ResultSet rs=stmt.executeQuery();
			while(rs.next()) {
				
				token1=rs.getString("token");
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}
	
		finally {
			try {
				//closing connection with database
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//returns true if data inserted successfully otherwise return false
		return token1;
	}
	
}
