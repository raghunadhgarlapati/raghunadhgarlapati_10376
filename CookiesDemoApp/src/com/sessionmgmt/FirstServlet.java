package com.sessionmgmt;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.GenerateRandomString;

/**
 * Servlet implementation class FirstServlet
 */
@WebServlet("/FirstServlet")
public class FirstServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {

			response.setContentType("text/html");
			PrintWriter out = response.getWriter();

			String username = request.getParameter("userName");
			String password = request.getParameter("pwd");
			
			
			String randomToken = new TokenGenrator().displayStudent(username);
			if(username.equals("admin") && password.equals("admin")) {
				if(randomToken != null) {	
				}else {
					String token = new GenerateRandomString().generatingRandomString();
					
					new TokenGenrator().createStudent(token,username);
				    randomToken = new TokenGenrator().displayStudent(username);
				}
				
			}else {
				out.println("Please Enter Valid Username And password.");
			}
			Cookie ck = new Cookie("uname",randomToken );// creating cookie object
			response.addCookie(ck);// adding cookie in the response

			// creating submit button
			out.print("<form action='SecondServlet'>");
			out.print("<input type='submit' value='go'>");
			out.print("</form>");

			out.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
