package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServieImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;
		
	@Transactional
	@Override
	public Account searchAccountByID(int accno) {
		// TODO Auto-generated method stub
		return accountDAO.getAccountByID(accno);
	}

	@Override
	@Transactional
	public boolean insert(Account account) {
		
		boolean res=accountDAO.insert(account);
		return res;
	}

}
