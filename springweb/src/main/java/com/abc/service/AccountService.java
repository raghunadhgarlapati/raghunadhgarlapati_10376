package com.abc.service;

import com.abc.hibernate.entities.Account;

public interface AccountService {

	Account searchAccountByID(int accno);

	boolean insert(Account account);

}
