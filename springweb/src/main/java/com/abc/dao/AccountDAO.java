package com.abc.dao;

import com.abc.hibernate.entities.Account;

public interface AccountDAO {

	Account getAccountByID(int accno);

	boolean insert(Account account);
}
