package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;
	
	@RequestMapping("/accounts/{id}")
	public String searchAccount(@PathVariable("id") int accno,ModelMap map) {
		Account account = accountService.searchAccountByID(accno);
		map.addAttribute("account", account);
		return "account_saved";
	}
	
	@GetMapping("/insert")
	public String getLoginForm() {
		return "studentinsert";
	}
	@PostMapping("/student")
	public String insertAccount(@ModelAttribute Account account,ModelMap map)
	{
		
		boolean result=accountService.insert(account);
		if(result)
		{
			return "inserted";
		}
		else
		{
			return "fail";
		}
		
}



}




