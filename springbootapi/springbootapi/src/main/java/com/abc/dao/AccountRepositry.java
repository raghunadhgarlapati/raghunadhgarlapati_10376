package com.abc.dao;

import org.springframework.data.repository.CrudRepository;

import com.abc.entites.Account;

public interface AccountRepositry  extends CrudRepository<Account, Integer>{

}
