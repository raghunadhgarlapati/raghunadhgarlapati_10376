package com.abc.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.dao.AccountRepositry;
import com.abc.entites.Account;
@Service
@Transactional
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	private AccountRepositry accountRepositry;
	@Override
	public Account save(Account account) {
		
		return accountRepositry.save(account);
	}

	@Override
	public List<Account> findAll() {
		// TODO Auto-generated method stub
		 List<Account> accountList = new ArrayList<Account>();
//		 repository.findAll().forEach(e -> accountList.add(e));
		 Iterable<Account> iAccount = accountRepositry.findAll();
		 Iterator<Account> iterator = iAccount.iterator();
		
		 while(iterator.hasNext()) {
			 accountList.add(iterator.next());
		 }
		 return accountList;
	}

	@Override
	public Account findAccountById(int accno) {
		// TODO Auto-generated method stub
		Optional<Account> account =accountRepositry.findById(accno);
		return  account.get();
	}

	@Override
	public void deleteAccount(int accno) {
		// TODO Auto-generated method stub
		accountRepositry.deleteById(accno);
		
	}

	
	

}
