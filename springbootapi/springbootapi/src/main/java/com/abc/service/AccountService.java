package com.abc.service;

import java.util.List;

import com.abc.entites.Account;

public interface AccountService {
	
public Account save(Account account);
	
	public List<Account> findAll();
	
	public Account findAccountById(int accno);
	
	public void deleteAccount(int accno);

}
