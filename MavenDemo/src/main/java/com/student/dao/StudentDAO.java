package com.student.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.student.bean.Student;
import com.student.util.HibernateUtil;

public class StudentDAO {
	public boolean create(Student student)
	{
//		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Configuration cfg=new Configuration();
		cfg.configure();
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(student);
		System.out.println(" student inseted");
		transaction.commit();
		session.close();
		return true;
	}

	public boolean delete(Student student) {
		SessionFactory sessionFactory=HibernateUtil.getSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		session.delete(student);
		System.out.println("student deleted");
		transaction.commit();
		session.close();
		return true;
	}
	

}
