package com.student.service;

import com.student.bean.Student;
import com.student.dao.StudentDAO;

public class StudentService {
	StudentDAO studentDAO=new StudentDAO();
	public boolean createService(Student student)
	{
		boolean flag=false;
		flag=studentDAO.create(student);
		return flag;
		
	}
	public boolean deleteStudent(Student student)
	{
		boolean flag=false;
		flag=studentDAO.delete(student);
		return flag;
	}

}
