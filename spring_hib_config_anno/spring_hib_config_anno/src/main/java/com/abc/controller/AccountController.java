package com.abc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.bean.Account;
import com.abc.service.AccountService;
import com.abc.service.AccountServiceImpl;

@Controller

public class AccountController {
	
	
	@Autowired
	private AccountService accountService;
	
	@RequestMapping("/accountsearch/{id}")
	public String searchAccount(@PathVariable("id") int id,ModelMap map) {
		Account account=accountService.searchById(101);
		map.addAttribute("account", account);
		
		return "account";
		
	}
	@GetMapping("/create")
	public String insert() {
		return "accountinsert";
	}
	@PostMapping("/add")
	public String createAccount(@ModelAttribute Account account, ModelMap map) {
		
		map.addAttribute("account", account);
		
		boolean result=accountService.addAccount(account);
		if(result) {
		
		return "account";
	}
	return null;
	}
	@GetMapping("/update")
	public String upadteAccount() {
		return "accountinsert";
	}
	@PostMapping("/accountUpdation")
	public String updateAccount(@ModelAttribute Account account, ModelMap map) {
		
		int accno =	account.getAccno();
		String name=account.getName();
		double balance=account.getBalance();
		
		 account=accountService.searchById(accno);
		
		if(account!=null) {
		
		account.setAccno(accno);
		
		account.setName(name);
	
		account.setBalance(balance);
		
		map.addAttribute("account", account);
		
		accountService.updateAccount(account);
		
		
		return "update_success";
		
		}
	return "error";
	}
	
	@RequestMapping("/accountdelete/{id}")
	public String deleteAccount(@PathVariable("id") int id,ModelMap map) {
		Account account=accountService.searchById(id);
		boolean result=false;
		if(account!=null) {
			result=accountService.deleteAccount(account);
			result=true;
		}
		map.addAttribute("account", account);
		
		if(result) {
			
			return "delete_success";
		}
		return "error";
		
	}
	
	@RequestMapping("/displayAccounts")
	public String displayAccounts(ModelMap map){
		
		List<Account> accounts = accountService.displayAccountDetails();
		map.addAttribute("accounts", accounts);
		
		return "display";
	}
}
