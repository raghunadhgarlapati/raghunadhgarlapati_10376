package com.abc.DAO;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.bean.Account;
@Repository
public class AccounDAOImpl  implements AccountDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Account getAccountByAccNo(int accno) {
		
		Session session = sessionFactory.getCurrentSession();
		
		Account account=session.get(Account.class, accno);
		
		return account;
	}

	@Override
	public boolean createAccount(Account account) {

		Session session = sessionFactory.getCurrentSession();
		  
		session.save(account);
		
		return true;
	}

	@Override
	public boolean modifyAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		
		Account account1=new Account();
		
		if(account.getAccno()!=0) {
			account1.setAccno(account.getAccno());
			
		}
		if(account.getName()!=null) {
			account1.setName(account.getName());
			
		}
		if(account.getBalance()!=0) {
			account1.setBalance(account.getBalance());
			
		}
		session.update(account1);
		
		return true;
	}
	

	@Override
	public boolean removeAccount(Account account) {

		Session session = sessionFactory.getCurrentSession();
		  
		session.delete(account);
		
		return true;
	}

	
	public List<Account> displayDetails(){
		
		Session session = sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Account.class);
		List<Account> results = crit.list();
		
		return results;
	}
}
