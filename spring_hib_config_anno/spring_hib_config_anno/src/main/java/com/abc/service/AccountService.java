package com.abc.service;

import java.util.List;

import com.abc.bean.Account;

public interface AccountService {

	Account searchById(int accNo);

	boolean addAccount(Account account);

	boolean updateAccount(Account account);
	
	boolean deleteAccount(Account account);
	
	List<Account> displayAccountDetails();
}
