package com.abc.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.abc.dao.StudentDao;
import com.abc.pojo.StudentDetails;

@Service
public class StudentServices {
	
	@Autowired
	StudentDao  studentDao;
	public StudentDao getStudentDao() {
		return studentDao;
	}
	public void setStudentDao(StudentDao studentDao) {
		this.studentDao = studentDao;
	}
	public boolean register(StudentDetails details) {
		
		StudentDetails details2=new StudentDetails();
		details2.setPassword(details.getPassword());
		boolean result=studentDao.register(details2);
		return result;
	}

}
