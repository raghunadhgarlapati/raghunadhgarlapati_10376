package com.abc.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.abc.pojo.StudentDetails;
import com.abc.services.StudentServices;

@Controller
public class StudentController {
	@GetMapping("Student")
	public String  registerStudent()
	{
		return "student";
		
	}
	@Autowired
	StudentServices studentServices;
	
	@PostMapping("/studentregister")
	public String registerTheStudent(@ModelAttribute StudentDetails details,ModelMap map)
	{
	   
	  boolean result=studentServices.register(details);
	   
	   if(result)
	   {
		   map.addAttribute("email", details.getEmail());
		   return "registersucessful";
		   
	   }
	   else
	   {
		   return "fail";
		   
	   }
		
	}
	

}
