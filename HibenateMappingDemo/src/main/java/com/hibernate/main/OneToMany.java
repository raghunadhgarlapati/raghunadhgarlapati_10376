package com.hibernate.main;

import java.util.ArrayList;
import java.util.List;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.hibernate.mapping.Cart;
import com.hibernate.mapping.Item;
import com.hibernate.util.HibernateUtil;

public class OneToMany {

	public static void main(String[] args) {
		Cart cart=new Cart();
		cart.setName(" VarunCart");
		Item item1=new Item();
		item1.setQuantity(100);
		item1.setItemTotal(500);
		
	    item1.setId(1);
	    item1.setCart1(cart);
	    
	    
	    Item item2=new Item();
	    
	    item2.setQuantity(200);
	    item2.setItemTotal(600);
	    item2.setCart1(cart);
	    
	    List<Item>items=new ArrayList<Item>();
	    items.add(item1);
	    items.add(item2);
	    
	    cart.setItems(items);
	    SessionFactory factory=HibernateUtil.getSessionFactory();
	    Session  session=factory.openSession();
	    Transaction txn=session.beginTransaction();
	    session.save(cart);
	    session.save(item1);
	    session.save(item2);
	   
	    System.out.println("Sucess");
	    txn.commit();
	    
	    
	}
	

}
