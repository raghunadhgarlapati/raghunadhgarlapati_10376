package com.retailbillingsystem.provider;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.retailbillingsystem.bean.Invoice_details;
import com.retailbillingsystem.bean.Products;
import com.retailbillingsystem.dao.InvoiceDao;
import com.retailbillingsystem.dao.ProductsDao;
import com.retailbillingsystem.services.BillerServices;

@Path("/biller")
public class BillerProvider {
	
	@POST
	@Path("/billgenerate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<String> billGenerate(Products product_array[])
	{
		BillerServices billerServices = new BillerServices();
		List<Products> products = Arrays.asList(product_array);
		List<Products> productList = ProductsDao.readFromTable_products();
		ArrayList<String> billingList = new ArrayList<String>();
		ArrayList<Invoice_details> invoice_details = new ArrayList<Invoice_details>();
		Invoice_details details = new Invoice_details();
		int invoice_num=0;
		List<Invoice_details> invoiceDetails=InvoiceDao.readFromTable_invoice();
		//initializing invoice number
		if(invoiceDetails.isEmpty())
		{
			invoice_num=1001;
		}
		else
		{
			invoice_num=invoiceDetails.get(invoiceDetails.size()-1).getInvoiceno(); 
			invoice_num++;
		}
		
		double total=0;
		for(Products product : products)
		{
			for(Products product1 : productList)
			{
				if(product.getProductname().equalsIgnoreCase(product1.getProductname()))
				{
					product.setPrice(billerServices.price_calculation(product.getProductname(), product.getQuantity()));
					total=billerServices.total_calculation(product.getPrice(), total);
					billingList.add("Product Name= "+product.getProductname());
					billingList.add("Product Quantity= "+product.getQuantity());
					billingList.add("Product Price= "+product.getPrice());
					details.setInvoiceno(invoice_num);
					details.setPrice(product.getPrice());
					details.setQuantity(product.getQuantity());
					details.setProduct_id(product1.getProduct_id());
					invoice_details.add(details);
				}
			}
		}
		
		double tax = billerServices.tax_caluculation(total);
		double discount = billerServices.discount_calculation(total);
		
		billerServices.billingProducts(invoice_details);
		
		billingList.add("Total ="+total);
		billingList.add("Tax= "+tax);
		billingList.add("Discount = "+discount);
		
		return billingList;
	}
	
}
