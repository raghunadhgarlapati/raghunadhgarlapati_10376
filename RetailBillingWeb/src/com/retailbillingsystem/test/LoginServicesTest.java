package com.retailbillingsystem.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.retailbillingsystem.services.LoginServices;

public class LoginServicesTest {

	LoginServices services = null;
	
	@Before
	public void setUp() {
		
		services = new LoginServices();
		
	}
	
	@After
	public void tearBack() {
		
		services = null;
	}
	
	@Test
	public void managerLoginPositiveTest() {
		
		boolean flag = services.managerLogin("Puran","aspire");
		assertTrue(flag);
	}
	
	@Test
	public void managerLoginNegativeTest() {
		
		boolean flag = services.managerLogin("puran","aspi123e");
		assertFalse(flag);
	}
	
	@Test
	public void stockerLoginPositiveTest() {
		
		boolean flag = services.stockerLogin("seshu","seshu");
		assertTrue(flag);
	}
	
	@Test
	public void stockerLoginNegativeTest() {
		
		boolean flag = services.stockerLogin("seshu","12345");
		assertFalse(flag);
	}
	
	@Test
	public void billerLoginPositiveTest() {
		
		boolean flag = services.billerLogin("chandu","chandu1");
		assertTrue(flag);
	}
	
	@Test
	public void billerLoginNegativeTest() {
		
		boolean flag = services.billerLogin("puran","aspi123e");
		assertFalse(flag);
	}
	
}
