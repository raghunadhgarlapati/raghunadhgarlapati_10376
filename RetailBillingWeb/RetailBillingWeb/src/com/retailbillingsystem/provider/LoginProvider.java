package com.retailbillingsystem.provider;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import com.retailbillingsystem.services.LoginServices;



@Path("/login")
public class LoginProvider {
	
	@Path("/managerLogin")
	@GET
	public String managerLogin(@QueryParam("emp_name") String username, @QueryParam("emp_password") String password) {
		boolean managerLogin = new LoginServices().managerLogin(username, password);
		String res = managerLogin ? "Login successfull...." : "please enter valid details";
		return res;
	}
	
	@Path("/stockerLogin")
	@GET
	public String stockerLogin(@QueryParam("emp_name") String username, @QueryParam("emp_password") String password) {
		boolean stockerLogin = new LoginServices().managerLogin(username, password);
		String res = stockerLogin ? "Login successfull...." : "please enter valid details";
		return res;
	}
	
	@Path("/billerLogin")
	@GET
	public String billerLogin(@QueryParam("emp_name") String username, @QueryParam("emp_password") String password) {
		boolean billerLogin = new LoginServices().managerLogin(username, password);
		String res = billerLogin ? "Login successfull...." : "please enter valid details";
		return res;
	}
	
}
