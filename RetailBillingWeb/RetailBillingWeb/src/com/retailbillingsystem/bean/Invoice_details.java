package com.retailbillingsystem.bean;

/***
 * This class is for storing the previous price details
 * @author BATCH A
 *
 */
public class Invoice_details {
	
	//initializing the variables
	private int invoiceno;
	private double quantity;
	private double price;
	private int product_id;
	
	/***
	 * This method is for getting invoiceno of the current bill
	 * @return invoiceno
	 */
	public int getInvoiceno() {
		return invoiceno;
	}
	
	/***
	 * This method is for setting invoiceno of the current bill
	 * @param invoiceno
	 */
	public void setInvoiceno(int invoiceno) {
		this.invoiceno = invoiceno;
	}
	
	/***
	 * This method is for getting quantity of the current bill
	 * @return quantity
	 */
	public double getQuantity() {
		return quantity;
	}
	
	/***
	 * This method is for setting quantity of the current bill
	 * @param quantity
	 */
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	/***
	 * This method is for getting price of the current bill
	 * @return price
	 */
	public double getPrice() {
		return price;
	}
	
	/***
	 * This method is for setting price of the current bill
	 * @param price
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	/***
	 * This method is for getting product_id of the current bill
	 * @return product_id
	 */
	public int getProduct_id() {
		return product_id;
	}
	
	/***
	 * This method is for setting product_id of the current bill
	 * @param product_id
	 */
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	
	
}
