package com.retailbillingsystem.services;

import java.util.List;

import com.retailbillingsystem.bean.EmployeeDetails;
import com.retailbillingsystem.dao.EmployeeDao;

/**
 * This class is used to allow employees to login
 * @author Batch-A
 *
 */
public class LoginServices {
	
	/**
	 * This method is used to check the credentials given by the manager and allow him to login
	 * @param username
	 * @param password
	 * @return true if credentials matches
	 */
	public boolean managerLogin(String username,String password)
	{
//
//		EmployeeDao dao = new EmployeeDao();
//		dao.createManager();
//		
		List<EmployeeDetails> employeeDetails = EmployeeDao.fetchEmployeeDetails();
		boolean flag = false;
		for(EmployeeDetails details : employeeDetails)
		{
			if((username.equals(details.getEmployeeName())) && (password.equals(details.getEmployeePassword())) && details.getRole().equalsIgnoreCase("Manager"))
			{
				
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * This method is used to check the credentials given by the stocker and allow him to login
	 * @param username
	 * @param password
	 * @return true if credentials matches
	 */
	public static boolean stockerLogin(String username, String password) {

		List<EmployeeDetails> employeeDetails = EmployeeDao.fetchEmployeeDetails();
		boolean flag = false;
		for(EmployeeDetails details : employeeDetails)
		{
			if((username.equals(details.getEmployeeName())) && (password.equals(details.getEmployeePassword())) && details.getRole().equalsIgnoreCase("stocker"))
			{
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * This method is used to check the credentials given by the biller and allow him to login
	 * @param username
	 * @param password
	 * @return true if credentials matches
	 */
	public static boolean billerLogin(String username, String password) {
		List<EmployeeDetails> employeeDetails = EmployeeDao.fetchEmployeeDetails();
		boolean flag = false;
		for(EmployeeDetails details : employeeDetails)
		{
			if((username.equals(details.getEmployeeName())) && (password.equals(details.getEmployeePassword())) && details.getRole().equalsIgnoreCase("biller"))
			{
				flag = true;
			}
		}
		return flag;
	}

}