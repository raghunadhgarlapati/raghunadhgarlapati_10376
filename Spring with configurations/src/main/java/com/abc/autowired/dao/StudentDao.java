package com.abc.autowired.dao;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.autowired.bean.Adress;
import com.abc.autowired.bean.Student;
import com.abc.autowired.util.HibernateUtil;
@Repository
public class StudentDao {
	
	@Autowired
	Student student;
	@Autowired
	Adress adress;

	public Student display() {
//		adress.setCity("Eluru");
//		adress.setHouseNo(122);
		
//		student.setName("raghu");
//		student.setAge(21);
//		student.setRollno(1003);
		
//		student.setAdress(adress);
		return student;
		
		
		
	}
	public void insert() {
		student.setName("raghu");
//		student.setAge(21);
//		student.setRollno(1005);
		SessionFactory factory=HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		session.save(student);
		transaction.commit();
		System.out.println("sucess");
		
		
	}
	public Student displayDb()
	{
		SessionFactory factory=HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		Student student=(Student) session.createCriteria(Student.class);
		return student;
	}

}
