package com.abc.autowired.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.abc.autowired.bean.Student;
import com.abc.autowired.services.StudentServices;

@Controller
public class StudentController {
	
	@Autowired
	StudentServices services;

	

	public Student display() {
		
		Student student=services.display();
		return student;
	}



	public StudentServices getServices() {
		return services;
	}



	public void setServices(StudentServices services) {
		this.services = services;
	}
	
	public void insert()
	{
		services.insert();
	}
	public Student displayDb() {
		Student  student=services.displayDb();
		return student;
	
	}

}
