package com.abc.autowired.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
@ComponentScan(basePackages = {"com.abc.autowired"})
@PropertySource("com/abc/autowired/properties/student.properties")
public class StudentConfiguration {
  
	
	
}
