package com.abc.autowired.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import com.abc.autowired.bean.Student;
import com.abc.autowired.configuration.StudentConfiguration;
import com.abc.autowired.controller.StudentController;



public class StudentMain {


		public static void main(String[] args) {
			
			ApplicationContext context = new AnnotationConfigApplicationContext(StudentConfiguration.class);
	 
		         StudentController controller = context.getBean(StudentController.class);
		         
		
				Student  student =controller.display();
				//controller.insert();
				
				//Student student2=controller.displayDb();
				
				// System.out.println(student2);
				
				
			  	System.out.println("StudentDetails");
		  	System.out.println(student.getName());
			  	
		}

}
