package com.presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Source
 */
@WebServlet("/Source")
public class Source extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Source() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		//session creation
		HttpSession session = request.getSession();
		//adding element to session
		session.setAttribute("myemail", email);
		//invalidating the session with 10 seconds
		session.setMaxInactiveInterval(10);
		
		
		
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		out.println("Hello :"+firstName+" "+lastName+"<br>");
		out.println("<a href='Target'>Click here to See your email adress</a>");
		out.println("</body></html>");
		out.close();
	}

}
