package com.abc.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.abc.bean.Message;

public class HelloMain {

	public static void main(String[] args) {
		 ApplicationContext context =  new ClassPathXmlApplicationContext("classpath:com/abc/xml/config.xml");
	      
		
		Message message=(Message) context.getBean("msg");
		message.setMsg("raghu");
		message.display();
	}

}
